import '@babel/polyfill';
import 'mutationobserver-shim';
import Vue from 'vue';
import './plugins/bootstrap-vue';
import App from './App.vue';
import router from './router';
import store from './store';
import BootstrapVue from 'bootstrap-vue';
import firebase from 'firebase';
import vuescroll from 'vue-scroll';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import scroll from './directives/scroll';
import * as VueGoogleMaps from 'vue2-google-maps';
import Vuelidate from 'vuelidate';

const moment = require('moment');
require('moment/locale/es');
Vue.use(Vuelidate);
Vue.directive('scroll', scroll);
Vue.use(require('vue-moment'), {
	moment,
});
Vue.use(VueGoogleMaps, {
	load: {
		key: 'AIzaSyBth3OBSoZJIqiUmEgz8ehRUgqB0pd7mMM',
		libraries: 'places',
	},
});

Vue.use(BootstrapVue);

Vue.use(vuescroll, { debounce: 600 });

Vue.config.productionTip = false;

var firebaseConfig = {
	apiKey: 'AIzaSyByQJZGnAB7GLxfvuASdlr-B9-TLBx4MOM',
	authDomain: 'uploadimage-408af.firebaseapp.com',
	databaseURL: 'https://uploadimage-408af.firebaseio.com',
	projectId: 'uploadimage-408af',
	storageBucket: 'uploadimage-408af.appspot.com',
	messagingSenderId: '796175853616',
	appId: '1:796175853616:web:96d937525c590bcd62a5f5',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

router.beforeEach((to, from, next) => {
	if (to.matched.some((record) => record.meta.requireAuth)) {
		if (!store.state.isLogged) next('/');
		else next();
	} else {
		next();
	}
});

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount('#app');
