import Vue from 'vue';
import Vuex from 'vuex';
import { service } from '../src/api';
import { log } from 'util';
Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		club: {},
		currentUser: {},
		currentClub: {},
		clubes: [],
		event: {
			name: '',
			description: '',
			date: '',
			price: '',
			img: '',
		},
		list: [],
		events: [],
		counters: { showForm: false, loader: false },
		prueba: [],
		isLogged: null || sessionStorage.getItem('log'),
	},
	mutations: {
		['SET_USER'](state, payload) {
			state.currentUser = payload;
			state.isLogged = true;
			sessionStorage.setItem('user', JSON.stringify(payload));
			sessionStorage.setItem('log', true);
		},
		['SET_CLUB'](state, payload) {
			state.currentClub = payload;
			//   console.log('el actual club', state.currentClub);
		},
		['SET_CLUBES'](state, payload) {
			state.clubes = payload;
		},
		['SET_LIST'](state, payload) {
			state.list = payload;
		},
		['SET_CURRENT_CLUB'](state, payload) {
			state.club = payload;
		},
	},
	actions: {
		async login({ commit }, payload) {
			let res = await service.login(payload);
			//   console.log('res del store', res.data);
			commit('SET_USER', res.data);
			sessionStorage.setItem('userId', res.data.data.idAdministrador);
			return res.data;
		},
		async register({ commit }, payload) {
			let res = await service.newAdmin(payload);
			//   console.log('soy el res del registeruser', res.data.data);
			commit('SET_USER', res.data.data);
			sessionStorage.setItem('userId', res.data.data.idAdministrador);
			return res.data;
		},
		async createClub({ commit }, payload) {
			let res = await service.newClub(payload);
			commit('SET_CLUB', res.data);
			return res.data;
		},
		club({ commit }, payload) {
			//   console.log('payload', payload);
			commit('SET_CLUB', payload);
			return payload;
		},
		async pullclubes({ commit }) {
			let res = await service.allClubes();
			commit('SET_CLUBES', res.data);
			return res.data;
		},
		async clubById({ commit }, payload) {
			let res = await service.eventByClub(payload);
			return res.data;
		},
		async getClubById({ commit }, payload) {
			let res = await service.getClubById(payload);
			console.log('res del clb', res);

			commit('SET_CURRENT_CLUB', res.data);
		},
		async getEventsByClub({ commit }, payload) {
			let res = await service.getEventsByClub(payload);
		},
		async updateClub({ commit }, payload) {
			//   console.log('este', payload);

			let res = await service.updateClub(payload);
			//   console.log(res.data);
		},
		async deleteEvent({ commit }, payload) {
			//   console.log('payload del store', payload);
			// let res = await service.deleteEvent(payload);
		},

		async updateEvent({ commit }, payload) {
			let res = await service.updateEvent(payload);
		},
		async promotionsByClub({ commit }) {
			let res = await service.promotionByClub();
			return res.data;
		},

		async deletePromotion({ commit }, payload) {
			console.log('payload del store', payload);
			// let res = await service.deleteEvent(payload);
			let res = await service.deletePromotion(payload);
		},
		async getListByClub({ commit }, payload) {
			//   console.log('payload', payload);

			let res = await service.getListByClub(payload);
			commit('SET_LIST', res.data);
			console.log('listas', res);
		},
	},
});
