const axios = require('axios');
var client;
var baseURL =
	'https://frozen-fortress-60394.herokuapp.com/rest.v1.servicio.funny/';
function Service() {
	if (!client) {
		client = axios.create({
			baseURL: baseURL,
			timeout: 5000,
			headers: {
				'Access-Control-Allow-Origin':
					'https://frozen-fortress-60394.herokuapp.com/rest.v1.servicio.funny/',
				'Access-Control-Allow-Methods':
					'GET, POST, PATCH, PUT, DELETE, OPTIONS',
			},
		});
	}
	this.login = function(creds) {
		return client.get(
			`/administrador.by.correo.pass/?correo=${creds.correo}&pass=${creds.pass}`
		);
	};

	this.newAdmin = function(creds) {
		return client.post('/validar.registro.administrador/', creds);
	};
	this.newClub = function(creds) {
		// console.log("somos los creds del club", creds);
		return client.post('/club.post/', creds);
	};

	this.getClubById = function(creds) {
		return client.post(`/adminandclub.by.id/${creds}`);
	};

	this.updateClub = function(creds) {
		return client.put(`/club.put/`, creds);
	};

	this.allClubes = function() {
		return client.get('/all.clubes/');
	};

	this.newEvent = function(creds) {
		// console.log(creds);
		return client.post('eventos.post/', creds);
	};
	this.deleteEvent = function(creds) {
		return client.delete(`/eventos.delete/${creds}`);
	};
	this.updateEvent = function(creds) {
		return client.put(`/eventos.put/`, creds);
	};
	this.eventByClub = function() {
		return client.get(
			`/eventos.get.idLocal/${sessionStorage.getItem('userId')}`
		);
	};
	this.promotionByClub = function() {
		return client.get(`/promociones/${sessionStorage.getItem('userId')}`);
	};

	this.newPromotion = function(creds) {
		// console.log("somos los creds del club", creds);
		return client.post('/promocion.post/', creds);
	};

	this.updatePromotion = function(creds) {
		// console.log("somos los creds del club", creds);
		return client.put('/promocion.put/', creds);
	};
	this.deletePromotion = function(creds) {
		// console.log("somos los creds del club", creds);
		return client.delete(`/promocion.delete/${creds}`);
	};

	this.getListByClub = function(creds) {
		return client.get(`/listas.by.clubs/${creds}`);
	};
}

export const service = new Service();
